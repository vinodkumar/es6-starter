require("babel-polyfill");

// Reduce
const myReduce = (fun, init) =>
  function*(coll) {
    let result = init;
    for (const num of coll) yield (result = fun(result, num));
  };

const add = (x, y) => x + y;

myReduce(add, 0)([1, 2, 3, 4]);

// Map
const map = fun =>
  function*(coll) {
    for (const val of coll) yield fun(val);
  };

const inc = x => x + 1;

map(inc)([1, 2, 3, 4, 5]);

// filter
const filter = fun =>
  function*(coll) {
    for (const val of coll) if (fun(val)) yield val;
  };

const even = x => x % 2 === 0;
filter(even)([1, 2, 3, 4, 5]);

// max
const max = (...coll) => coll.reduce((acc, val) => (acc > val ? acc : val));

max(-1, 2, -3);

// unique elements

const getUnique = (acc, val) => {
  if (acc.indexOf(val) === -1) acc.push(val);
  return acc;
};

const unique = arr => arr.reduce(getUnique, []);

unique([1, 3, 4, 1, 2, 7, 3, 4]);

// toarray

const toArray = (...args) => args.reduce((prev, curr) => [...prev, curr], []);

toArray(1, 2, 3, 4, 5);

const sum = (...x) => x.reduce((acc, val) => acc + val);
sum(1, 2, 3);

const neg = x => -x;

const add10 = x => x + 10;

// pipeLine
const pipeLine = (...funs) =>
  funs.reduce((fun1, fun2) => (...args) => fun2(fun1(...args)));

pipeLine(sum, neg, add10)(1, 2, 3, 4, 5);

// comp
const comp = (...funs) =>
  funs.reverse().reduce((fun1, fun2) => (...args) => fun2(fun1(...args)));

comp(add10, neg, sum)(1, 2, 3, 4, 5);
